module.exports = function (app) {

	app.get('/api/getDate', function (req, res) {
		var ifModifiedSinceString = req.get("if-modified-since");
		if (ifModifiedSinceString) {
			res.sendStatus(304);
			return;
		}

		res.setHeader('Cache-Control', 'private');
		res.setHeader('Last-Modified', new Date().toUTCString());
		res.json(Date().toString());
	});
};